<?php
# ChernPIX Contact Info Widget #

class Ch_About_Us_Widget extends WP_Widget {

	function __construct() {
		$widget_ops = array( 'classname' => 'ch-about-us-widget',);
		parent::__construct( 'ch-about-us-widget', 'Niroon "About Us" widget', $widget_ops );
        }
        function form( $instance ) {
                $ch_about_wg_title         = isset($instance['ch-about-widget-title']) ? $instance['ch-about-widget-title'] : null;
                $ch_about_wg_logo_id       = isset($instance['ch-about-widget-logo-id']) ? $instance['ch-about-widget-logo-id'] : null;
                $ch_about_wg_text_content  = isset($instance['ch-about-widget-textcontent']) ? $instance['ch-about-widget-textcontent'] : null;
              ?> 

       <p>
        <label for="<?php echo esc_attr($this->get_field_id( 'ch-about-widget-title') ); ?>"><?php esc_html_e( 'Title :','niroon' ); ?></label>
        <input class="widefat" id="<?php echo esc_attr($this->get_field_id( 'ch-about-widget-title' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'ch-about-widget-title' )); ?>" type="text" value = '<?php echo esc_attr($ch_about_wg_title); ?>'/>
       </p>

        <p>
        <label for="<?php echo esc_attr($this->get_field_id( 'ch-about-widget-logo-id') ); ?>"><?php esc_html_e( 'Logo Address :','niroon' ); ?></label>
        <input class="widefat" id="<?php echo esc_attr($this->get_field_id( 'ch-about-widget-logo-id' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'ch-about-widget-logo-id' )); ?>" type="text" value = '<?php echo esc_attr($ch_about_wg_logo_id); ?>'/>
       </p>

       <p>
        <label for="<?php echo esc_attr($this->get_field_id( 'ch-about-widget-textcontent') ); ?>"><?php esc_html_e( 'About Text : ','niroon' ); ?></label>
        <textarea class="widefat" id="<?php echo esc_attr($this->get_field_id( 'ch-about-widget-textcontent' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'ch-about-widget-textcontent' )); ?>" ><?php echo esc_html($ch_about_wg_text_content); ?></textarea>
       </p>

             <?php
         }
	function widget( $args, $instance ) {
		 ?>
        <div class = 's-one-widget '>
                        <div class = 'widget-content'>
                                <?php if(strlen($instance['ch-about-widget-title']) > 0): ?>
                                  <span class = 's-widget-title'><?php echo esc_html($instance['ch-about-widget-title']); ?></span>
                                <?php endif ; ?>
                                
                                <?php if(strlen($instance['ch-about-widget-logo-id']) > 10) : ?>
                                  <img src = '<?php echo esc_url($instance['ch-about-widget-logo-id']); ?>' class = 'ch-wgt-about-img' />
                                <?php endif ; ?>

                                <?php if(strlen($instance['ch-about-widget-textcontent']) >  0) : ?>
                                   <span class = 'ch-wgt-about-textcontent'>
                                      <?php echo esc_html($instance['ch-about-widget-textcontent']) ; ?>
                                   <span>
                                <?php endif ; ?>
                                
                       </div>
                 </div><!-- end widget -->
		 <?php
	}
        function update( $new_instance, $old_instance ) {
         $old_instance['ch-about-widget-title'] = $new_instance['ch-about-widget-title'];
         $old_instance['ch-about-widget-logo-id'] = $new_instance['ch-about-widget-logo-id'];
         $old_instance['ch-about-widget-textcontent']     = $new_instance['ch-about-widget-textcontent'];
          
         return $old_instance; 
         }
}
add_action( 'widgets_init', function(){ register_widget( 'Ch_About_Us_Widget' ); } );

?>